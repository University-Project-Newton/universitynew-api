const express = require('express')
const routers = express.Router()
const conn = require('../model/db.js')
const bcrypt = require("bcrypt")

// Example Route
routers.get('/', async (req, res) => { // routers.<method>('<name of route>', async(req, res) => {
    const user = await conn.query('SELECT * FROM User') // const <var> = await conn.query('SQL')
    console.log(user) // Print Check
    res.json(user) // Send Data Back To Front-End and Show in Thunder Client
})
routers.post('/sign-up', async (req, res) => {
    let user = req.body.user
    let pass = req.body.pass
    let email = req.body.email 
    
    if (user === "" || pass === "" || email === "") {
        console.log("sign-up-fail");
    } else {
        const password = pass
        const hash = await bcrypt.hash(password,12)
       
        await conn.query('INSERT INTO User (Usercol, Passcol, Mailcol) VALUES (?, ?, ?)', [user, hash, email])
        res.status(200).json({ "msg": "sign-up successfully" })
    }

})
module.exports = routers;