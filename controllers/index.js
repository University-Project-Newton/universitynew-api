const express = require('express')
const routers = express.Router()
const authRoute = require('./auth')

routers.use('/auth', authRoute)

module.exports = routers;