const express = require("express")
const cors = require('cors')
const dotenv = require('dotenv')
const app = express()

const apiRoutes = require('./controllers')

dotenv.config()

// access config var

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))

app.get("/", (req, res) => {
    res.send("Hello! Node.js")
})

app.use('/api', apiRoutes);

module.exports = app;
