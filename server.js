const PORT = 8080;
const app = require("./app");
const http = require("http");
const server = http.createServer(app);

server.listen(PORT, "0.0.0.0", () => {
  console.log(`Service listening on port ${PORT}`);
});
